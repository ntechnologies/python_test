'''Used to retrieve data from github
'''
import subprocess
from subprocess import STDOUT
import os
import os.path


def checkout(path, ref):
    '''Checkout the github repository.'''
    subprocess.Popen(
        ['/usr/bin/git', 'reset', '--hard', ref],
        cwd=path,
        stderr=STDOUT).wait()


def fetch(path):
    '''Fetch the data from the github'''
    subprocess.Popen(
        ['/usr/bin/git', 'fetch'],
        cwd=path,
        stderr=STDOUT).wait()


def clone(path, repository):
    '''Clone github repository'''
    subprocess.Popen(
        ['/usr/bin/git', 'clone', repository, os.path.abspath(path)],
        stderr=STDOUT).wait()
